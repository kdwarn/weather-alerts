# weather-alerts

A set of programs to get and view weather alerts.

src/bin/fetch_api_data.rs will store the geojson feature collection in local file.
  * A .env file is required, containing a unique string for the USER_AGENT (e.g. `USER_AGENT="something_unique"`) for authentication to the API. The NWS plans to replace this with an API key in the future. See <https://www.weather.gov/documentation/services-web-api>.

src/bin/explore_data.rs is the initial work to parse that file and explore it.
  * one of the following is required:
    * a CSV of geographies to filter for (see <https://www.census.gov/geographies/reference-files/2019/demo/popest/2019-fips.html> for constructing the codes) - e.g. `042091`
    * a CSV of alert types to filter for
