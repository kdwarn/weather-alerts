//! Program for parsing the FeatureCollection geojson file stored by the
//! fetch_api_data program.
//! It takes an optional command line argument of a filename where configuration
//! data is stored.
use std::env;
use std::fmt::Write;
use std::fs::{self, File};
use std::path::PathBuf;
use std::process;

use geojson::FeatureCollection;
use serde::Deserialize;

#[derive(Deserialize)]
struct Config {
    id: String,
    email: String,
}

#[derive(Deserialize)]
struct Geography {
    state: String,
    county: String,
    code: String,
}

#[derive(Deserialize)]
struct Alert {
    name: String,
}

fn main() {
    // read config file passed in on command line
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Error. Please provide a path to the user id.");
        println!("e.g. \"explore_data user/ID_HERE\"");
        process::exit(1);
    }
    let cfg_file = format!("{}/config.toml", &args[1]);
    let contents = fs::read_to_string(cfg_file).expect("Something went wrong reading the file");
    let config: Config = toml::from_str(&contents).unwrap();

    // get options from geography and/or alert CSV files
    let mut geography_csv_path = PathBuf::new();
    let mut alert_csv_path = PathBuf::new();
    geography_csv_path.push(format!("user/{}/geography.csv", config.id));
    alert_csv_path.push(format!("user/{}/alert.csv", config.id));

    if !geography_csv_path.exists() && !alert_csv_path.exists() {
        println!("Found neither a geography CSV nor an alert CSV. At least one must be availble.");
        process::exit(1);
    }

    // create vector of counties to filter for
    let mut counties: Vec<String> = vec![];
    if geography_csv_path.exists() {
        let geography_csv = File::open(geography_csv_path).unwrap();
        let mut rdr = csv::Reader::from_reader(geography_csv);

        for result in rdr.deserialize() {
            let record: Geography = result.unwrap();
            counties.push(record.code);
        }
    }

    // create vector of alerts to filter for
    let mut alerts: Vec<String> = vec![];
    if alert_csv_path.exists() {
        let alert_csv = File::open(alert_csv_path).unwrap();
        let mut rdr = csv::Reader::from_reader(alert_csv);

        for result in rdr.deserialize() {
            let record: Alert = result.unwrap();
            alerts.push(record.name);
        }
    }

    // read from file stored by fetch_api_data program
    let active_alerts = fs::read_to_string("active_alerts.geojson")
        .unwrap()
        .parse::<FeatureCollection>()
        .unwrap()
        .features;

    let mut filtered_alerts = vec![];
    'outer: for active_alert in &active_alerts {
        // check against the types of alerts we're looking for
        // and skip rest of loop if not one of them
        if !alerts.is_empty() {
            let mut found = false;
            for alert in &alerts {
                if active_alert.property("event").unwrap() == alert {
                    found = true;
                }
            }
            if !found {
                continue 'outer;
            }
        }

        // check if it's in the geography we're looking for
        if !counties.is_empty() {
            if let Some(geocodes) = active_alert.property("geocode") {
                for fips in geocodes["SAME"].as_array().unwrap() {
                    for county in &counties {
                        if fips.to_string().trim_matches('"') == county {
                            filtered_alerts.push(active_alert.clone());
                            // multiple counties we want may be in same alert,
                            // so if one matches we can skip checking the rest
                            continue 'outer;
                        }
                    }
                }
            }
        }
    }

    if !filtered_alerts.is_empty() {
        // build and save a markdown file out of filtered_alerts
        let mut contents = "# Alerts\n\n".to_string();
        for alert in &filtered_alerts {
            write!(&mut contents, "## {:?}\n\n", &alert.id).unwrap();
        }
        let md_path = format!("user/{}/filtered_alerts.md", config.id);
        fs::write(md_path, contents).unwrap();

        // build a FeatureCollection out of filtered_alerts and save it as geojson file
        let fc = FeatureCollection::from_iter(filtered_alerts.into_iter());
        let path = format!("user/{}/filtered_alerts.geojson", config.id);
        let output = File::create(path).unwrap();
        serde_json::to_writer(output, &fc).unwrap();
    } else {
        println!("No alerts found for criteria given in CSV file(s).")
    }
}
