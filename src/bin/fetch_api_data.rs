//! This program fetches the entire FeatureCollection geojson
//! and stores it locally, so we don't have to repeatedly hit the API
use std::fs::File;

use dotenv_codegen::dotenv;
use geojson::FeatureCollection;
use reqwest::header::USER_AGENT;

fn main() {
    let url = "https://api.weather.gov/alerts/active";
    let data = fetch_data(url.to_string()).unwrap();
    let path = "active_alerts.geojson";
    let output = File::create(path).unwrap();
    serde_json::to_writer(output, &data).unwrap();
}

fn fetch_data(url: String) -> Result<FeatureCollection, reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let res = client
        .get(url)
        .header(USER_AGENT, dotenv!("USER_AGENT"))
        .send()?;

    let feature_collection = res.text()?.parse::<FeatureCollection>().unwrap();

    Ok(feature_collection)
}
